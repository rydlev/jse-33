package ru.t1.rydlev.tm.dto.response;

import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
public abstract class AbstractResponse implements Serializable {
}
