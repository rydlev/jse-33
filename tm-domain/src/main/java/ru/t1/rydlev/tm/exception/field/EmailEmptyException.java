package ru.t1.rydlev.tm.exception.field;

public class EmailEmptyException extends AbstractFieldException {

    public EmailEmptyException() {
        super("Error! Email is empty...");
    }

}
