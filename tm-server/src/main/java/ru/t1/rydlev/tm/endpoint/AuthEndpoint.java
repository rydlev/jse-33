package ru.t1.rydlev.tm.endpoint;

import io.swagger.annotations.ApiParam;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rydlev.tm.api.endpoint.IAuthEndpoint;
import ru.t1.rydlev.tm.api.service.IAuthService;
import ru.t1.rydlev.tm.api.service.IServiceLocator;
import ru.t1.rydlev.tm.dto.Result;
import ru.t1.rydlev.tm.dto.request.UserLoginRequest;
import ru.t1.rydlev.tm.dto.request.UserLogoutRequest;
import ru.t1.rydlev.tm.dto.request.UserViewProfileRequest;
import ru.t1.rydlev.tm.dto.response.UserLoginResponse;
import ru.t1.rydlev.tm.dto.response.UserLogoutResponse;
import ru.t1.rydlev.tm.dto.response.UserViewProfileResponse;
import ru.t1.rydlev.tm.model.Session;
import ru.t1.rydlev.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@NoArgsConstructor
@Path("/api/AuthEndpoint")
@WebService(endpointInterface = "ru.t1.rydlev.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/ping")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Result ping() {
        return new Result();
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/loginUser")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UserLoginResponse loginUser(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRequest request
    ) {
        @NotNull final IAuthService authService = getServiceLocator().getAuthService();
        @NotNull final String token = authService.login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/logoutUser")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UserLogoutResponse logoutUser(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRequest request
    ) {
        final Session session = check(request);
        getServiceLocator().getAuthService().invalidate(session);
        return new UserLogoutResponse();
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/viewUserProfile")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UserViewProfileResponse viewUserProfile(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserViewProfileRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final User user = getServiceLocator().getUserService().findOneById(userId);
        return new UserViewProfileResponse(user);
    }

}
