package ru.t1.rydlev.tm.api.repository;

import ru.t1.rydlev.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
