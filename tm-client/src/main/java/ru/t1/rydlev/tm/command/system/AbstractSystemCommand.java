package ru.t1.rydlev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rydlev.tm.api.endpoint.ISystemEndpoint;
import ru.t1.rydlev.tm.api.service.ICommandService;
import ru.t1.rydlev.tm.api.service.IPropertyService;
import ru.t1.rydlev.tm.command.AbstractCommand;
import ru.t1.rydlev.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @NotNull
    protected IPropertyService getPropertyService() {
        return serviceLocator.getPropertyService();
    }

    @NotNull
    protected ISystemEndpoint getSystemEndpoint() {
        return serviceLocator.getSystemEndpointClient();
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
