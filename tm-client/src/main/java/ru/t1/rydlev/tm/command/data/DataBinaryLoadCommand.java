package ru.t1.rydlev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.dto.request.DataBinaryLoadRequest;

public class DataBinaryLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-bin";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("DATA BINARY LOAD");
        @NotNull final DataBinaryLoadRequest request = new DataBinaryLoadRequest(getToken());
        getDomainEndpointClient().loadDataBinary(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load data from binary file";
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
