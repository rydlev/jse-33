package ru.t1.rydlev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.dto.request.DataYamlLoadFasterXmlRequest;

public final class DataYamlLoadFasterXmlCommand extends AbstractDataCommand {

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("DATA LOAD YAML");
        @NotNull final DataYamlLoadFasterXmlRequest request = new DataYamlLoadFasterXmlRequest(getToken());
        getDomainEndpointClient().loadDataYamlFasterXml(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load data from yaml file";
    }

    @NotNull
    @Override
    public String getName() {
        return "data-load-yaml";
    }

}
