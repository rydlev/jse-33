package ru.t1.rydlev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.command.AbstractCommand;

import java.util.Collection;

public final class ArgumentListCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final AbstractCommand command : commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-a";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show developer arguments.";
    }

    @NotNull
    @Override
    public String getName() {
        return "arguments";
    }

}
